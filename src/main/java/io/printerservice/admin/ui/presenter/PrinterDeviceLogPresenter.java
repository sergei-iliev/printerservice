package io.printerservice.admin.ui.presenter;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.printerservice.admin.backend.entity.PrinterDeviceLog;
import io.printerservice.admin.backend.repository.PrinterDeviceLogRepository;

@Component
public class PrinterDeviceLogPresenter {
     
	@Autowired
	private PrinterDeviceLogRepository printerDeviceLogRepository;
	
	public Collection<PrinterDeviceLog> getPrinterDeviceLogs(){		
		return printerDeviceLogRepository.findAll();			
	}

	public int getPrinterDeviceLogsCount(){
		return 0;
	}

}
