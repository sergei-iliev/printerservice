package io.printerservice.admin.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import io.printerservice.admin.backend.entity.PrinterDeviceLog;
import io.printerservice.admin.ui.presenter.PrinterDeviceLogPresenter;

@Route(value = "printer/report", layout = MainView.class)
@PageTitle("Printer Report")
public class PrinterReportView extends VerticalLayout{

	 @Autowired
	 private PrinterDeviceLogPresenter printerDeviceLogPresenter;
	 
	 private Grid<PrinterDeviceLog> grid;
	 
	 public PrinterReportView(){
		 createGrid();
		
	 }
	 
	 @Override
	protected void onAttach(AttachEvent attachEvent) {	
		super.onAttach(attachEvent);
		
		load();
	}
	 private void createGrid() {
		 grid=new Grid<PrinterDeviceLog>();
		

	     grid.addColumn(PrinterDeviceLog::getDate).setResizable(true).setHeader("Date");

	     grid.addColumn(PrinterDeviceLog::getDeviceId).setResizable(true).setHeader("DeviceId");

	     grid.addColumn(PrinterDeviceLog::getFirmaware).setResizable(true).setHeader("Firmaware");
	     
	     grid.addColumn(PrinterDeviceLog::getHostIdentification).setResizable(true).setHeader("Host Identification");

	     grid.addColumn(PrinterDeviceLog::getPagesPrinted).setResizable(true).setHeader("Pages Printed");
	     
	     grid.addColumn(PrinterDeviceLog::getTotalPrintCount).setResizable(true).setHeader("Total Print Count");
	     
	     grid.addColumn(PrinterDeviceLog::getTotalPrintLength).setResizable(true).setHeader("Total Page Length");
	     	        
	     this.add(grid);
	    
	 }
	 
	 private void load(){		    		    
			grid.setItems(printerDeviceLogPresenter.getPrinterDeviceLogs());			 
	 }
}
