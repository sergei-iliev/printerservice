package io.printerservice.admin.backend.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "printer_device_log")
public class PrinterDeviceLog extends AbstractEntity{

	  @Column(name="device_id")
	  private String deviceId;  //device.unique_id
	  /*
	   * pages since the last roll was changes
	   */
	  @Column(name="pages_printed")
	  private String pagesPrinted;    //odometer.user_label_count
	  
	  @Column(name="host_identification")
	  private String hostIdentification;   //device.host_identification
	  
	  
	  @Column(name="total_print_length")
	  private String totalPrintLength;   //odometer.headnew "3024 INCHES, 7681 CENTIMETERS"
	  
	  /*
	   * total pages count printed on this printer	  
	   */
	  @Column(name="total_print_count")
	  private String totalPrintCount;  //odometer.media_marker_count  
	  
	  
	  private String firmaware;   //appl.name
	  
	  private Date date=new Date();

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPagesPrinted() {
		return pagesPrinted;
	}

	public void setPagesPrinted(String pagesPrinted) {
		this.pagesPrinted = pagesPrinted;
	}

	public String getHostIdentification() {
		return hostIdentification;
	}

	public void setHostIdentification(String hostIdentification) {
		this.hostIdentification = hostIdentification;
	}

	public String getTotalPrintLength() {
		return totalPrintLength;
	}

	public void setTotalPrintLength(String totalPrintLength) {
		this.totalPrintLength = totalPrintLength;
	}

	public String getTotalPrintCount() {
		return totalPrintCount;
	}

	public void setTotalPrintCount(String totalPrintCount) {
		this.totalPrintCount = totalPrintCount;
	}

	public String getFirmaware() {
		return firmaware;
	}

	public void setFirmaware(String firmaware) {
		this.firmaware = firmaware;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	  
	  
	  
	  
}
