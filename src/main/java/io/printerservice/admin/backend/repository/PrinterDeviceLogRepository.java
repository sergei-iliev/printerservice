package io.printerservice.admin.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.printerservice.admin.backend.entity.PrinterDeviceLog;

@Repository
public interface PrinterDeviceLogRepository extends JpaRepository<PrinterDeviceLog,Long>{

}
