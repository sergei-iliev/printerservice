package io.printerservice.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.printerservice.admin.backend.entity.PrinterDeviceLog;
import io.printerservice.admin.backend.repository.PrinterDeviceLogRepository;

@RestController
public class PrinterController {

	@Autowired
	private PrinterDeviceLogRepository printerDeviceLogRepository;
	
	@GetMapping("/")
	public String hello() {
		return "Hello world - springboot-appengine-standard!";
	}	
	
	@PostMapping("/zebra") 
	public ResponseEntity<Void> ZebraRestTest(@RequestBody String body) {
		System.out.println(body);
	    String[] tokens=body.split("\\|");

	    
	    PrinterDeviceLog log=new PrinterDeviceLog();
	    log.setDeviceId(tokens[0]);
	    log.setPagesPrinted(tokens[1]);
	    log.setHostIdentification(tokens[2]);
	    log.setTotalPrintLength(tokens[3]);
	    log.setTotalPrintCount((tokens[4]));
	    log.setFirmaware(tokens[5]);
	    printerDeviceLogRepository.save(log);
	    
		return ResponseEntity.noContent().build();
	}
	
}
