package io.printerservice.admin;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.printerservice.PrinterServiceApplication;
import io.printerservice.admin.backend.entity.PrinterDeviceLog;
import io.printerservice.admin.backend.repository.PrinterDeviceLogRepository;

@SpringBootTest
class VigilantZebraServicesApplicationTests {

	@Autowired
	private PrinterDeviceLogRepository printerDeviceLogRepository;
	
	@Test
	void testSplitInput() {
	    String input="D3J191201620|431|ZD620-300dpi,V84.20.15,12,8192KB|3024 INCHES, 7681 CENTIMETERS|724|V84.20.15";
	    String[] tokens=input.split("\\|");

	    
	    PrinterDeviceLog log=new PrinterDeviceLog();
	    log.setDeviceId(tokens[0]);
	    log.setPagesPrinted(tokens[1]);
	    log.setHostIdentification(tokens[2]);
	    log.setTotalPrintLength(tokens[3]);
	    log.setTotalPrintCount((tokens[4]));
	    log.setFirmaware(tokens[5]);
	    printerDeviceLogRepository.save(log);
	   
	    
	}

}
